package ru.edu;

public class TextAnalyzerImpl implements TextAnalyzer {

    /**
     * Контейнер статистики.
     */
    private TextStatistics statistics = new TextStatistics();

    /**
     * Анализ строки произведения.
     *
     * @param line
     */
    @Override
    public void analyze(final String line) {
        statistics.addCharsCount(line.length());

        char current;
        boolean isWord = false;
        for (int i = 0; i < line.length(); i++) {
            current = line.charAt(i);

            if (Character.isSpaceChar(current)) {
                if (isWord) {
                    statistics.addWordsCount(1);
                }
                isWord = false;
                continue;
            }
            statistics.addCharsCountWithoutSpaces(1);

            if (Character.isLetterOrDigit(current)) {
                isWord = true;
            } else /*if (current != '\n')*/ {
                statistics.addCharsCountOnlyPunctuations(1);
            }
        }
        if (isWord) {
            statistics.addWordsCount(1);
        }
    }

    /**
     * Получение сохраненной статистики.
     *
     * @return TextStatistic
     */
    @Override
    public TextStatistics getStatistic() {
        return statistics;
    }

}
