package ru.edu;

import org.junit.Test;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;

public class StatisticReporterImplTest {

    public static final String RESULT_TXT = "./target/reporter_result.txt";

    @Test
    public void writeStats() {
        StatisticReporter reporter = new StatisticReporterImpl(RESULT_TXT);

        TextStatistics statistics = new TextStatistics();

        statistics.addCharsCount(100);
        statistics.addWordsCount(50);
        statistics.addCharsCountOnlyPunctuations(25);
        statistics.addCharsCountWithoutSpaces(10);

        reporter.report(statistics);

        List<String> lines = readFile(RESULT_TXT);
        assertEquals(4, lines.size());
        assertEquals("Words: 50", lines.get(0));
        assertEquals("Chars: 100", lines.get(1));
        assertEquals("NotSpace: 10", lines.get(2));
        assertEquals("Puncts: 25", lines.get(3));


    }

    private List<String> readFile(String resultTxt) {
        List<String> lines = new ArrayList<>();
        try (FileReader fr = new FileReader(resultTxt);
             BufferedReader br = new BufferedReader(fr)) {
            String line;
            while ((line = br.readLine()) != null) {
                lines.add(line);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return lines;
    }
}
