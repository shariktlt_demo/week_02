package ru.edu;

import org.junit.Test;
import org.mockito.InOrder;
import org.mockito.Mock;
import org.mockito.Spy;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.*;
import static org.mockito.MockitoAnnotations.openMocks;

public class FileSourceReaderTest {

    private final static String EXIST_FILE = "./src/test/resources/input_text.txt";
    private final static String TEST_FILE = "./src/test/resources/testSourceReader.txt";
    public static final String NOT_EXIST_FILE_PATH = "./not_exist_file_path";

    private SourceReader reader = new FileSourceReader();

    @Mock
    private TextAnalyzer mockAnalyzer;

    @Spy
    private TextStatistics mockStatistic = new TextStatistics();

    @Test
    public void setupExistFilePath() {
        reader.setup(EXIST_FILE);
    }

    @Test(expected = IllegalArgumentException.class)
    public void setupNull() {
        reader.setup(null);
    }

    @Test(expected = IllegalArgumentException.class)
    public void setupNotExistFile() {
        reader.setup(NOT_EXIST_FILE_PATH);
    }

    @Test
    public void readSource() {
        //Явно создаем моки и шпионы
        //TextAnalyzer mockAnalyzer = mock(TextAnalyzer.class);
        //TextStatistics mockStatistic = spy(new TextStatistics());

        openMocks(this);

        /**
         * Идентичные формы записи
         */
        when(mockAnalyzer.getStatistic()).thenReturn(mockStatistic);
        //ниже используется для методов с void возвращаемым типом
        doReturn(mockStatistic).when(mockAnalyzer).getStatistic();

        reader.setup(TEST_FILE);
        TextStatistics statistics = reader.readSource(mockAnalyzer);

        assertEquals(mockStatistic, statistics);


        /**
         * Проверяем сколько раз с каким аргументом вызывали заглушку. Порядок не учитывается
         */
        verify(mockAnalyzer, times(1)).analyze("first line");
        verify(mockAnalyzer, times(1)).analyze("\nsecond line");

        verify(mockAnalyzer, times(2)).analyze(anyString());
        verify(mockAnalyzer, times(2)).analyze(any(String.class));
        /*
            если 1 аргумент любая строка, 2 аргумент конкретная строка
            verify(mockAnalyzer, times(2)).analyze(any(String.class), eq("asd"));
         */

        InOrder inOrder = inOrder(mockAnalyzer);

        /**
         * Ниже уже в определенной последовательности проверяем вызовы
         */
        inOrder.verify(mockAnalyzer, times(1)).analyze("first line");
        inOrder.verify(mockAnalyzer, times(1)).analyze("\nsecond line");


        /**
         * Проверяем что объект статистики не вызывался
         */
        verify(mockStatistic, times(0)).addCharsCountWithoutSpaces(anyInt());
    }



}