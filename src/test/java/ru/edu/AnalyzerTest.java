package ru.edu;

import org.junit.Test;
import ru.edu.empty.EmptySourceReader;
import ru.edu.empty.EmptyStatisticReporter;
import ru.edu.empty.EmptyTextAnalyzer;

import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class AnalyzerTest {

    /**
     * По умолчанию задачи запускаются с рабочей директорией в корне проекта
     */
    public static final String FILE_PATH = "./src/test/resources/input_text.txt";
    public static final int EXPECTED_WORDS = 266;
    public static final int EXPECTED_CHARS = 2062;
    public static final int EXPECTED_CHARS_WO_SPACES = EXPECTED_CHARS - 409 + 30;
    public static final int EXPECTED_CHARS_ONLY_PUNCT = 84;
    public static final String RESULT_PATH = "./result.txt";

    /**
     * Нужно переключиться на вашу реализацию
     */
    private final TextAnalyzer analyzer = new TextAnalyzerImpl();

    /**
     * Нужно переключиться на вашу реализацию
     */
    private final SourceReader reader = new FileSourceReader();

    private final StatisticReporter reporter = new StatisticReporterImpl(RESULT_PATH);


    @Test
    public void validation() {
        reader.setup(FILE_PATH);//132a
        TextStatistics statistics = reader.readSource(analyzer);

        assertNotNull(statistics);

        System.out.println(statistics);

        assertEquals(EXPECTED_WORDS, statistics.getWordsCount());
        assertEquals(EXPECTED_CHARS, statistics.getCharsCount());
        assertEquals(EXPECTED_CHARS_WO_SPACES, statistics.getCharsCountWithoutSpaces());
        assertEquals(EXPECTED_CHARS_ONLY_PUNCT, statistics.getCharsCountOnlyPunctuations());

        List<String> topWords = statistics.getTopWords();

        reporter.report(statistics);


        assertEquals(0, topWords.size());

    }

}